FROM node:20.12.1
# libvips-dev for sharp compatability
RUN apt-get update && apt-get -y upgrade libvips libvips-dev

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

WORKDIR /opt/
COPY ./package.json ./
COPY ./yarn.lock ./

ENV PATH /opt/node_modules/.bin:$PATH
RUN yarn config set network-timeout 600000 -g
RUN yarn install

WORKDIR /opt/app
COPY ./ .

RUN yarn build
EXPOSE 1337

CMD ["yarn", "dev"]

module.exports = ({ env }) => ({
  // email: {
  //   config: {
  //     provider: 'sendgrid',
  //     providerOptions: {
  //       apiKey: env('SENDGRID_API_KEY'),
  //     },
  //     settings: {
  //       defaultFrom: 'no-reply@mports.io',
  //       defaultReplyTo: 'help@mports.io',
  //       testAddress: 'mohaiminul.sust@gmail.com',
  //     },
  //   },
  // },
  "users-permissions": {
    config: {
      jwtSecret: env('JWT_SECRET'),
      providers: {
        authentik: {
          enabled: true,
          icon: 'authentik',
          key: env('AUTHENTIK_CLIENT_ID'),
          secret: env('AUTHENTIK_CLIENT_SECRET'),
          callback: `${env('STRAPI_URL')}/auth/authentik/callback`,
          authEndpoint: `${env('AUTHENTIK_URL')}/oauth/authorize`,
          tokenEndpoint: `${env('AUTHENTIK_URL')}/oauth/token`,
          userEndpoint: `${env('AUTHENTIK_URL')}/oauth/userinfo`,
          scope: ['openid', 'profile', 'email'],
        },
      },
    },
  },
  upload: {
    config: {
      sizeLimit: 500 * 1024 * 1024, // max upload size
      provider: 'local', //cloudinary
      providerOptions: {
        // cloud_name: env('CLOUDINARY_NAME'),
        // api_key: env('CLOUDINARY_KEY'),
        // api_secret: env('CLOUDINARY_SECRET'),
        localServer: {
          maxage: 300000
        },
      },
      actionOptions: {
        upload: {},
        delete: {},
      },
      // provider: 'aws-s3',
      // providerOptions: {
      //   accessKeyId: env('AWS_ACCESS_KEY_ID'),
      //   secretAccessKey: env('AWS_ACCESS_SECRET'),
      //   region: env('AWS_REGION'),
      //   params: {
      //     Bucket: env('AWS_BUCKET'),
      //   },
      // },
      breakpoints: {
        xlarge: 1920,
        large: 1000,
        medium: 750,
        small: 500,
        xsmall: 64
      },
    },
  },
  "entity-notes": {
		enabled: true,
	},
  publisher: {
		enabled: true,
		config: {
			hooks: {
				beforePublish: async ({ strapi, uid, entity }) => {
					console.log('beforePublish');
				},
				afterPublish: async ({ strapi, uid, entity }) => {
					console.log('afterPublish');
				},
				beforeUnpublish: async ({ strapi, uid, entity }) => {
					console.log('beforeUnpublish');
				},
				afterUnpublish: async ({ strapi, uid, entity }) => {
					console.log('afterUnpublish');
				},
			},
		},
	},
  seo: {
    enabled: true,
  },
  redis: {
    config: {
      connections: {
        default: {
          connection: {
            host: env('REDIS_HOST', 'redis'),
            port: env('REDIS_PORT', 6379),
            db: 0,
            // username: 'default',
            // password: 'secret',
          },
          settings: {
            debug: false,
          },
        },
      },
    },
  },
  // graphql: {
  //   config: {
  //     endpoint: '/graphql',
  //     shadowCRUD: true,
  //     playgroundAlways: false,
  //     depthLimit: 7,
  //     amountLimit: 100,
  //     apolloServer: {
  //       tracing: false, // make it false in future after dev work finishes
  //     },
  //   },
  // },
  "rest-cache": {
    config: {
      provider: {
        name: "redis",
        options: {
          max: 32767,
          connection: "default",
        },
      },
      strategy: {
        keysPrefix: "apiCache",
        maxAge: 3600,
        debug: false,
        contentTypes: [
          "api::category.category",
          "api::article.article",
          "api::publisher.publisher",
          "api::book.book",
          "api::tag.tag",
          "api::review.review",
        ],
      },
    },
  },
  transformer: {
    enabled: true,
    config: {
      prefix: '/api/',
      responseTransforms: {
        removeAttributesKey: true,
        removeDataKey: true,
      }
    }
  },
  sentry: {
    enabled: true,
    config: {
      dsn: env('SENTRY_DSN'),
      sendMetadata: true,
    },
  },
});

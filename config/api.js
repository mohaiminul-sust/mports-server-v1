module.exports = {
  rest: {
    defaultLimit: 30,
    maxLimit: 10e6,
    withCount: true,
  },
};

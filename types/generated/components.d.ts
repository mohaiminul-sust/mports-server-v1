import type { Schema, Attribute } from '@strapi/strapi';

export interface SharedAsideImage extends Schema.Component {
  collectionName: 'components_shared_aside_images';
  info: {
    displayName: 'AsidePhoto';
    icon: 'dice-six';
    description: '';
  };
  attributes: {
    photo: Attribute.Media & Attribute.Required;
    align_image_to_left: Attribute.Boolean & Attribute.DefaultTo<true>;
    text: Attribute.RichText;
  };
}

export interface SharedCaptionedPhotoGroup extends Schema.Component {
  collectionName: 'components_shared_captioned_photo_groups';
  info: {
    displayName: 'CaptionedPhotoGroup';
    icon: 'images';
  };
  attributes: {
    photos: Attribute.Media & Attribute.Required;
    short_desc: Attribute.Text;
  };
}

export interface SharedCaptionedPhoto extends Schema.Component {
  collectionName: 'components_shared_captioned_photos';
  info: {
    displayName: 'CaptionedPhoto';
    icon: 'camera-retro';
    description: '';
  };
  attributes: {
    photo: Attribute.Media & Attribute.Required;
    short_desc: Attribute.Text;
  };
}

export interface SharedCaptionedVideo extends Schema.Component {
  collectionName: 'components_shared_captioned_videos';
  info: {
    displayName: 'CaptionedVideo';
    icon: 'film';
    description: '';
  };
  attributes: {
    video: Attribute.Media & Attribute.Required;
    short_desc: Attribute.Text;
  };
}

export interface SharedLinkPreview extends Schema.Component {
  collectionName: 'components_shared_link_previews';
  info: {
    displayName: 'LinkPreview';
    icon: 'link';
  };
  attributes: {
    link: Attribute.String & Attribute.Required;
  };
}

export interface SharedMediaText extends Schema.Component {
  collectionName: 'components_shared_media_texts';
  info: {
    displayName: 'RichText';
    icon: 'align-left';
    description: '';
  };
  attributes: {
    text: Attribute.RichText & Attribute.Required;
  };
}

export interface SharedMetaSocial extends Schema.Component {
  collectionName: 'components_shared_meta_socials';
  info: {
    displayName: 'metaSocial';
    icon: 'project-diagram';
  };
  attributes: {
    socialNetwork: Attribute.Enumeration<['Facebook', 'Twitter']> &
      Attribute.Required;
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 60;
      }>;
    description: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 65;
      }>;
    image: Attribute.Media;
  };
}

export interface SharedScAudioPlaylist extends Schema.Component {
  collectionName: 'components_shared_sc_audio_playlists';
  info: {
    displayName: 'SCAudioPlaylist';
    icon: 'clipboard-list';
    description: '';
  };
  attributes: {
    mini: Attribute.Boolean & Attribute.DefaultTo<true>;
    theme: Attribute.Enumeration<['dark', 'light']> &
      Attribute.DefaultTo<'light'>;
    playlist_id: Attribute.BigInteger;
    short_desc: Attribute.Text;
  };
}

export interface SharedSeo extends Schema.Component {
  collectionName: 'components_shared_seos';
  info: {
    displayName: 'seo';
    icon: 'search';
  };
  attributes: {
    metaTitle: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 60;
      }>;
    metaDescription: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 50;
        maxLength: 160;
      }>;
    metaImage: Attribute.Media & Attribute.Required;
    metaSocial: Attribute.Component<'shared.meta-social', true>;
    keywords: Attribute.Text;
    metaRobots: Attribute.String;
    structuredData: Attribute.JSON;
    metaViewport: Attribute.String;
    canonicalURL: Attribute.String;
  };
}

export interface SharedSoundcloudAudioTrack extends Schema.Component {
  collectionName: 'components_shared_soundcloud_audio_tracks';
  info: {
    displayName: 'SCAudioTrack';
    icon: 'play';
    description: '';
  };
  attributes: {
    mini: Attribute.Boolean & Attribute.DefaultTo<true>;
    theme: Attribute.Enumeration<['dark', 'light']> &
      Attribute.DefaultTo<'light'>;
    track_id: Attribute.BigInteger;
    short_desc: Attribute.Text;
  };
}

export interface SharedTimelineText extends Schema.Component {
  collectionName: 'components_shared_timeline_texts';
  info: {
    displayName: 'TimelineText';
    icon: 'align-justify';
  };
  attributes: {
    time: Attribute.DateTime & Attribute.Required;
    text: Attribute.RichText & Attribute.Required;
  };
}

export interface SharedYoutubeVideo extends Schema.Component {
  collectionName: 'components_shared_youtube_videos';
  info: {
    displayName: 'YoutubeVideo';
    icon: 'video';
    description: '';
  };
  attributes: {
    video_url: Attribute.String & Attribute.Required;
    short_desc: Attribute.Text;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'shared.aside-image': SharedAsideImage;
      'shared.captioned-photo-group': SharedCaptionedPhotoGroup;
      'shared.captioned-photo': SharedCaptionedPhoto;
      'shared.captioned-video': SharedCaptionedVideo;
      'shared.link-preview': SharedLinkPreview;
      'shared.media-text': SharedMediaText;
      'shared.meta-social': SharedMetaSocial;
      'shared.sc-audio-playlist': SharedScAudioPlaylist;
      'shared.seo': SharedSeo;
      'shared.soundcloud-audio-track': SharedSoundcloudAudioTrack;
      'shared.timeline-text': SharedTimelineText;
      'shared.youtube-video': SharedYoutubeVideo;
    }
  }
}

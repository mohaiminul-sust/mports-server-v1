# 🚀 Comic Server
To store and manage pdfs, articles, media assets and publication data

## Local Development (Yarn)

### `develop`

```
npm run develop
# or
yarn develop
```

### `start`


```
npm run start
# or
yarn start
```

### `build`

Build admin panel
```
npm run build
# or
yarn build
```

## Local Development + Deployment (Docker)

### `build dev`
```
docker compose build
```

### `start dev`
```
docker compose up -d
```
### `build and deploy with prod configs`
```
docker compose -f docker-compose.prod.yml build
docker compose -f docker-compose.prod.yml up -d
```

<sub>Contact mohaiminul.sust@gmail.com for more details</sub>

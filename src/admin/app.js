import Logo from "./extensions/comic.png"
import Favicon from "./extensions/comic.ico"

export default {
  config: {
    // locales: [
    //   // 'ar',
    //   // 'fr',
    //   // 'cs',
    //   // 'de',
    //   // 'dk',
    //   'en',
    //   // 'es',
    //   // 'he',
    //   // 'id',
    //   // 'it',
    //   // 'ja',
    //   // 'ko',
    //   // 'ms',
    //   // 'nl',
    //   // 'no',
    //   // 'pl',
    //   // 'pt-BR',
    //   // 'pt',
    //   // 'ru',
    //   // 'sk',
    //   // 'sv',
    //   // 'th',
    //   // 'tr',
    //   // 'uk',
    //   // 'vi',
    //   // 'zh-Hans',
    //   // 'zh',
    // ],
    head: {
      favicon: Favicon,
    },
    auth: {
      logo: Logo,
    },
    menu: {
      logo: Logo,
    },
    translations: {
      en: {
        "app.components.LeftMenu.navbrand.title": "Comic Server",
        "app.components.LeftMenu.navbrand.workplace": "Dashboard",
        "Auth.form.welcome.subtitle": "Enter the dungeon...",
        "Auth.form.welcome.title": "Comic Server",
      },
    },
  },
  bootstrap(app) {
    console.log(app);
  },
};
